function Alumno(id, num_de_control, nombre, apellido_paterno,apellido_materno, grupo) {
    var _id = id;
    var _num_de_control = num_de_control;
    var _nombre = nombre;
    var _apellido_paterno = apellido_paterno;
    var _apellido_materno = apellido_materno;
    var _grupo = grupo;
    
    function _getid() {
      return _id;
    }
    function _setid(id) {
      _id = id;
    }
    function _getnum_de_control() {
      return _num_de_control;
    }
    function _setnum_de_control(num_de_control) {
      _num_de_control = num_de_control;
    }
    function _getnombre() {
      return _nombre;
    }
    function _setnombre(nombre) {
      _nombre = nombre;
    }
    }